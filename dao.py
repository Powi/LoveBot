#!/usr/bin/python3

import sqlite3

class Dao(object):

    DB_NAME = "lovebot.db"


    def __init__(self):
        self.conn = sqlite3.connect(self.DB_NAME)
        self.cur = self.conn.cursor()

    def close(self):
        self.conn.close()

    def dropTable(self):
        statement = 'DROP TABLE IF EXISTS {}'.format(self.TABLE_NAME,)
        self.cur.execute(statement)

    def delete(self, id=None):
        if id:
            statement = "DELETE FROM {} WHERE id=?".format(self.TABLE_NAME)
            self.cur.execute(statement, (id,))
            self.conn.commit()


    def count(self):
        statement = "SELECT COUNT(*) as count FROM {}".format(self.TABLE_NAME,)
        self.cur.execute(statement)
        return self.cur.fetchone()['count']

class YoutubeChannelDao(Dao):

    TABLE_NAME = "youtube_channel"

    def __init__(self):
        Dao.__init__(self)
        self.createTable()

    def createTable(self):
        statement = """CREATE TABLE IF NOT EXISTS {}(
            id integer primary key autoincrement,
            user text not null,
            user_id text,
            link text not null
        )""".format(self.TABLE_NAME)
        self.cur.execute(statement)

    # return id's last row added
    def push(self, channel=None):
        if channel:
          if(self.get(user=channel['user'])):
            return
          else:
            statement = "INSERT INTO {}(user, user_id, link) VALUES(?,?,?)".format(self.TABLE_NAME)
            self.cur.execute(statement, (channel['user'], channel['user_id'], channel['link']))
            self.conn.commit()
            return self.cur.lastrowid

    # return a blog instance
    def get(self, user=None, user_id=None):
        if user:
            statement = "SELECT link FROM {} WHERE user=?".format(self.TABLE_NAME)
            self.cur.execute(statement, (user,))
        elif user_id:
            statement = "SELECT link FROM {} WHERE user_id=?".format(self.TABLE_NAME)
            self.cur.execute(statement, (user_id,))
        
        fetch=self.cur.fetchone()
        if fetch is None:
          return ""
        return fetch[0]
      