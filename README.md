# LoveBot

## Pourquoi ?

### Automatisation des tâches

**LoveBot** permet d’automatiser certaines tâches. Notamment réordoner les salons, dire un message pré-écris,
enregistré dans le code de LoveBot, envoie une recherche vers un moteur de recherche, donner le site web, la chaîne youtube d’une personne, etc.
le but est vraiment de faciliter la tâche des utilisateurices.
Son but principal est vraiment l’automatisation.

### Jouer, extras

Certaines fonctions du bot ne sont là que dans un but de jeu, il n’y en a encore que très peu d’existante et pour raison de **flood**,
beaucoup ont été retirée provisoirement en attendant de mettre des choses au point : Restriction des salons, des utilisateurices,…
Une fonction permet de choisir quelque chose pseudo-aléatoirement¹, une autre permet de lancer des dés pseudo-aléatoires¹…

### Autre

D’autres tâches comme l’envoie d’un message « annonce » au nom du #haut-conseil, avoir la liste complète des Rôles ou des Salons avec leur ID,…

## Comment ?

Pour faire appel à une commande de LoveBot, il faut envoyer un message avec “!” suivi du nom de la commande ; suivi des «   Paramètres » de la commande.
Si vous n’avez pas compris, je vais détailler la commande “roll” pour bien vous expliquer.
|
### Commandes de jeu

#### “roll”

“roll” est une commande de lancer de dés pseudo-aléatoire¹. Pour ça, il faut lui passer **un** paramètres sous la forme “NdN”.
Où le premier “N” est le nombre de dés à lancer. le “d” n’est pas à modifier et le second “N” est le nombre de faces par dé.

Exemple : `!roll 1d6` va lancer un dé à six faces.
|
### Commandes d’automatisation

#### “order” – Réservé aux modos, Demi-dieuxes et Divinités

“order” remet les salons en ordre, tout simplement.

Utilisation : `!order`

#### “ddg” “ddgi” “ddga” “qw” “qwi” “wp” “wk” “ud”

Respectivement, ces commandes renvoient vers :
Duckduckgo / Duckduckgo image / Duckduckgo answer / Qwant / Qwant image / Wikipédia / Wiktionnary / Urban Dictionnary
Pour envoyer une phrase (ou des mots séparés par des espaces), pensez à mettre des doubles-guillemets droits, avant et après la phrase : `"exemple de phrase"`

Exemples :
* `!ddg "Comment bien transitionner ?"`
  Cherche « Comment bien transitionner ? » sur le moteur de recherche Duckduckgo.
* `!ddgi "Chatons mignons"
  Cherche des images de « Chatons mignons » sur le moteur de recherche Duckduckgo image.
* `!ddga "2+2"
  Cherche la réponse à « 2+2 » sur le moteur de recherche Duckduckgo answer.
* `!qw "Comment bien transitionner ?"
  Cherche « Comment bien transitionner ? » sur le moteur de recherche Qwant.
* `!qwi "Chatons mignons"
  Cherche des images de « Chatons mignons » sur le moteur de recherche Qwant image.
* `!wp "Personne transgenre"
  Cherche « Personne transgenre » sur Wikipédia.
* `!wk Mx
  Cherche « Mx » sur Wiktionnary.
* `!ud non-binaire
  Cherche « non-binaire » sur le Urban Dictionnary.

____________________________

¹En informatique, rien n’est vraiment aléatoire. Il existe des algorithmes qui vont chercher à être le plus aléatoire possible…
Pour autant, ce n’est pas du vrai aléatoire.