#!/usr/bin/python3

import discord
import asyncio
import random
import inspect
import time
import urllib.parse
import hashlib
from switch import Switch
from sitew import sitew
from discord.ext import commands

from unidecode import unidecode
from dao import YoutubeChannelDao

tokenFile = open('token','r')
token = tokenFile.readline()

voice = ""
player = ""
DDLA = "261027636006879235"
UNDEXION = "291643917504610314"
RDLA = "264345087520145410"

description = '''Je suis revenu·e, plus fort·e que jamais. Et surtout… Je suis écris en Python3.\n
Né·e en NodeJS, je suis resté·e enfermé dans un disque dur lui-même bloqué dans un PC en panne\n
Mais je suis reviendu, plus fort·e et surtout… En Python 3 !! Parce que vive le Python ! Pour avoir mon code, utilisez !git'''
bot = commands.Bot(command_prefix='!', description=description)

PASSWORD_HASHED = "646765966391c18f4de7983d286299f50b2828ed47a83a9e4a1111a78911bc70c91eaecce48850671770a8999d2602dd4f38768998b22ccf5518b2126a5534b1"

def _get_variable(name):
    stack = inspect.stack()
    try:
        for frames in stack:
            try:
                frame = frames[0]
                current_locals = frame.f_locals
                if name in current_locals:
                    return current_locals[name]
            finally:
                del frame
    finally:
        del stack

def _author():
  return _get_variable('_internal_author')

def _channel():
  return _get_variable('_internal_channel')

def _server():
  return _get_variable('_internal_channel').server

def _say(*message):
  yield from bot.say(" ".join(message))
  
def _respond(*message):
  yield from bot.send_message(_author()," ".join(message))
        
def _rank():
    author = _author()
    
    roles_id = []
    roles = author.roles
    for r in roles:
      roles_id.append(r.id)
    
    if _server().id == DDLA:
      rang = 0 if ('261201100684656662' in roles_id) else 1 if ('261201326577287181' in roles_id) else 2 if ('272859520009502730' in roles_id) else 3
    elif _server().id == RDLA:
      rang = 3
    elif _server().id == UNDEXION:
      rang = 0 if ('291652169386688514' in roles_id) else 3
    
    return rang

def getR(ID):
  
  listRoles = []
  for role in _server().roles:
    listRoles.append({"name": role.name, "id": role.id, "self": role})

  print("--")
  for role in listRoles:
    if role['id'] == ID:
      print(role['self'])
      return role['self']

@bot.event
@asyncio.coroutine
def on_ready():
    print('Connecté en tant que ')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    tokenFile.close()
    discord.opus.load_opus('libopus.so.0')
    

# Commandes de gestion du Discord
@bot.command()
@asyncio.coroutine
def placesPubliques(*message):
  Roles = [
      {'id' : '311210864055287808', 'name' : 'Débats', 		'salon' : '<#275291838456266752>'},
      {'id' : '311210775538696192', 'name' : 'Partages', 	'salon' : '<#267699457699741700>'},
      {'id' : '311210453412085760', 'name' : 'Coup de gueule', 	'salon' : '<#267007298566750208>'},
      {'id' : '311210723714007050', 'name' : 'Cocon Flood', 	'salon' : '<#264071348395966465>'},
      {'id' : '311210834884034561', 'name' : 'Culture', 	'salon' : '<#290765447236681728>'},
      {'id' : '311211047337852929', 'name' : 'Interrogations',  'salon' : '<#290766285376323584>'}
    ]
  """Places publiques"""
  author = _author()
  rolesToAdd = []
  if message[0] == "toutes":
    for role in Roles:
      rolesToAdd.append(getR(role['id']))
  elif message[0] == "seulement":
    valides = message[1:]
    for role in Roles:
      if role['salon'] in valides:
        rolesToAdd.append(getR(role['id']))
        
  elif message[0] == "sauf":
    invalides = message[1:]
    for role in Roles:
      if role['salon'] not in invalides:
        rolesToAdd.append(getR(role['id']))
  if message[0] in ["toutes","seulement","sauf"]:
    print(len(rolesToAdd))
    yield from bot.add_roles(author,*rolesToAdd)
  else:
    liste = ""
    for role in Roles:
      liste += role['salon'] + " "
    yield from bot.say('Utilisation :\n\
`!placesPubliques toutes` -- pour avoir tout les salons places publiques.\n\
`!placesPubliques seulement #salon1 #salon2 …` -- pour avoir seulement les places publiques définies.\n\
`!placesPubliques sauf #salon1 #salon2 …` -- pour avoir toute les places publiques, sauf celle indiquées.\n\n\
Voici la liste des salons « places publiques » : ' + liste + ".")

@bot.command()
@asyncio.coroutine
def test_command():
  exec("_say('test')")

@bot.command()
@asyncio.coroutine
def execute(*message):
  command = " ".join(message)
  exec(command)

@bot.command()
@asyncio.coroutine
def serveur():
  """Informe du serveur sur lequel vous êtes."""
  yield from bot.send_message(_author(),_server().name)

@bot.command()
@asyncio.coroutine
def rolesList():
  """Envoie la liste des rôles en privé"""
  listRoles = ""
  for role in _server().roles:
    listRoles += role.id + " = " + role.name + "\n"
  yield from bot.send_message(_author(),listRoles)
  
@bot.command()
@asyncio.coroutine
def salonList():
  """Envoie la liste des salons en privé"""
  listSalons = ""
  for salon in _server().channels:
    yield from bot.send_message(_author(),"{'name':'"+salon.name+"','id':'"+salon.id+"'}")

@bot.command()
@asyncio.coroutine
def voiceConnect(id,ytlink):
  chan = bot.get_channel(id)
  voice = yield from bot.join_voice_channel(chan)
  player = yield from voice.create_ytdl_player(ytlink)
  print('test')
  player.start()
  
@bot.command()
@asyncio.coroutine
def voice(command : str):
  with Switch(command) as case:
    if case('yt'):
      yield from bot.send_message(_author(),player.yt)
    if case('url'):
      yield from botsend_message(_author(),player.url)
    if case('download_url'):
      yield from bot.send_message(_author(),player.download_url)
    if case('title'):
      yield from bot.send_message(_author(),player.title)
    if case('description'):
      yield from bot.send_message(_author(),player.description)
    if case('url'):
      yield from bot.send_message(_author(),player.url)
    if case('duration'):
      yield from bot.send_message(_author(),player.duration)

@bot.command()
@asyncio.coroutine
def order(password=None):
    if(password):
      yield from bot.say('Le mot de passe n’est plus utile, je sais désormais reconnaître un·e modo/demi-dieuxe/divinité quand j’en vois un·e.')
    
    if(_rank() <= 2):
        salons = [
          {'name':'a-lire','id':'310558510171553794'},
          {'name':'haut-conseil','id':'262713917028040704'},
          {'name':'moderation','id':'262700352099450881'},
          {'name':'citoyennete','id':'280772927362695180'},
          {'name':'presentation','id':'261210834162810881'},
          {'name':'presentation-flood','id':'262957827977379841'},
          {'name':'general','id':'261027636006879235'},
          {'name':'vocal-general','id':'263264338545082369'},
          {'name':'debats-discussions','id':'275291838456266752'},
          {'name':'interrogations-questionnements','id':'290766285376323584'},
          {'name':'coup-de-gueule','id':'267007298566750208'},
          {'name':'cocon-flood','id':'264071348395966465'},
          {'name':'culture','id':'290765447236681728'},
          {'name':'partages-decouvertes','id':'267699457699741700'},
          {'name':'evenements-rencontres','id':'270522180696145920'},
          {'name':'annonces-entre-membres','id':'273555667762020352'},
          {'name':'etudes-devoirs-travail','id':'284344059689041920'},
          {'name':'informatique','id':'270904389676433418'},
          {'name':'nourriture','id':'340207884560236559'},
          {'name':'langue-allemand','id':'316218415532474368'},
          {'name':'langue-anglais','id':'315995011453943809'},
          {'name':'langue-espagnol','id':'316218377612034048'},
          {'name':'langue-esperanto','id':'315914298800996353'},
          {'name':'langue-italien','id':'327453450633871361'},
          {'name':'nonmixte-ace','id':'261212712581726208'},
          {'name':'nonmixte-aromantique','id':'262924673220608001'},
          {'name':'nonmixte-orientation-sexuelle-et-romantique','id':'261215739715911700'},
          {'name':'nonmixte-polya','id':'268449099563532288'},
          {'name':'nonmixte-asx','id':'335934511479259137'},
          {'name':'nonmixte-bdsm','id':'267057565194518528'},
          {'name':'nonmixte-personnes-racise-e-s','id':'261207225622593540'},
          {'name':'nonmixte-xenogenre','id':'282281240332206082'},
          {'name':'nonmixte-nonbinaire','id':'269784750674149377'},
          {'name':'nonmixte-trans','id':'272858843531182081'},
          {'name':'nonmixte-automutilations','id':'307901358193770518'},
          {'name':'nonmixte-addictions','id':'282605307790884875'},
          {'name':'nonmixte-tca','id':'338643553930444801'},
          {'name':'nonmixte-psychoatypie','id':'282616396544737290'},
          {'name':'nonmixte-neuroatypique','id':'261541996852674563'},
          {'name':'nonmixte-maladies-handicaps','id':'261206414242873365'},
          {'name':'nonmixte-veggie','id':'261216631974264832'},
          {'name':'adultes-18plus','id':'261268362317922305'},
          {'name':'education-sexuelle','id':'281202864582295553'},
          {'name':'sexe','id':'281207113186017281'},
          {'name':'sexe-hard-exhib','id':'282954536979660802'},
          {'name':'sujets-non-safes','id':'279634457906905088'},
          {'name':'annivs','id':'284109002151952384'},
          {'name':'tests','id':'311203817322577920'}
        ]

        for salon in salons:
          yield from bot.move_channel(bot.get_channel(salon['id']),salons.index(salon))
          yield from bot.send_message(bot.get_channel('311203817322577920'),salon['name'])
        yield from bot.send_message(_author(),'J’ai fini de trier les salons. :-)')
    else:
        yield from bot.send_message(_author(),"Vous n'avez pas l'accès à cette fonction : Vous n’avez pas le rang nécéssaire.")


# Raccourcis et recherches internets

@bot.command()
@asyncio.coroutine
def ud(*words : str):
    """Urban Dictionary"""
    rep=""
    for word in words:
        rep+="https://www.urbandictionary.com/define.php?term="+urllib.parse.quote(word)+"\n"
    yield from bot.say(rep)

@bot.command()
@asyncio.coroutine
def wp(*search : str):
    """Wikipédia"""
    rep=""
    for word in search:
        rep+="https://fr.wikipedia.org/w/index.php?search="+urllib.parse.quote(word)+"\n"
    yield from bot.say(rep)

@bot.command()
@asyncio.coroutine
def wk(*search : str):
    """Wiktionnary"""
    rep=""
    for word in search:
        rep+="https://fr.wiktionary.org/w/index.php?title=Spécial:Recherche&search="+urllib.parse.quote(word)+"\n"
    yield from bot.say(rep)

@bot.command()
@asyncio.coroutine
def ddg(*search : str):
    """duckduckgo"""
    rep=""
    for word in search:
        rep+="https://duckduckgo.com/?q="+urllib.parse.quote(word)+"&t=ffab&ia=web\n"
    yield from bot.say(rep)

@bot.command()
@asyncio.coroutine
def ddgi(*search : str):
    """duckduckgo images"""
    rep=""
    for word in search:
        rep+="https://duckduckgo.com/?q="+urllib.parse.quote(word)+"&t=ffab&ia=web&iax=1&ia=images\n"
    yield from bot.say(rep)


@bot.command()
@asyncio.coroutine
def ddga(*search : str):
    """duckduckgo images"""
    rep=""
    for word in search:
        rep+="https://duckduckgo.com/?q="+urllib.parse.quote(word)+"&t=ffab&ia=web&iax=1&ia=answer\n"
    yield from bot.say(rep)


@bot.command()
@asyncio.coroutine
def qw(*search : str):
    """qwant"""
    rep=""
    for word in search:
        rep+="https://www.qwant.com/?q="+urllib.parse.quote(word)+"&t=web\n"
    yield from bot.say(rep)

@bot.command()
@asyncio.coroutine
def qwi(*search : str):
    """qwant images"""
    rep=""
    for word in search:
        rep+="https://www.qwant.com/?q="+urllib.parse.quote(word)+"&t=images\n"
    yield from bot.say(rep)
    
@bot.command()
@asyncio.coroutine
def topic():
  chan = _get_variable("_internal_channel")
  topic = chan.topic
  yield from bot.send_message(_author,topic or "Il n’y a pas de topic sur ce salon.")
    


# Informations pour les nouveaulles sur le Discord
@bot.command()
@asyncio.coroutine
def presentation():
    """Envoie le message invitant à se présenter."""
    yield from bot.say("Vous pouvez vous présenter sur <#261210834162810881> et obtenir (si vous le voulez) des rôles (étiquettes) qui vous donnent accès aux salons nonmixtes dans lesquels vous êtes concerné·e·s.")

@bot.command()
@asyncio.coroutine
def pronom():
    """Envoie le message incitant à mettre un pronom."""
    yield from bot.say("Hey la tradition ici c'est de mettre son pronom entre parenthèses pour savoir comment s'adresser à toi, t'es pas obligé-e mais c'est cool de le faire, le fait que seules les personnes transgenres mettent leur pronom dans leur pseudo les marginalise.\n\
    Amis cisgenres soutenez vos camarades en mettant si vous le souhaitez votre pronom dans votre pseudo <:rainbow_heart:262928726570827776><:trans_heart:264133518936768512>😉🌼\n\
    Pour changer votre pseudo seulement sur ce Discord, faites un clic droit sur votre pseudo, dans la liste de personnes connectées sur la droite, puis cliquez sur « Changer de pseudo » et vous pouvez alors y ajouter vos pronoms 😉.")
    

@bot.command()
@asyncio.coroutine
def roles():
    roleFile = open('roles','r')
    roleList = ''.join(roleFile.readlines())
    roles = roleList.split('|')
    for role in roles:
      yield from bot.say(role)
    roleFile.close()

# Information en lien avec LoveBot

@bot.command()
@asyncio.coroutine
def description():
    """Envoie la description du bot."""
    yield from bot.say(bot.description)

@bot.command()
@asyncio.coroutine
def language():
    """Donne le language de programmation utilisé pour le développer."""
    yield from bot.send_message(_author(),'Python3. désormais, je suis écris en Python3')
    
@bot.command()
@asyncio.coroutine
def git():
    """Envoie le lien du git de LoveBot."""
    yield from bot.send_message(_author(),"<https://framagit.org/FirePowi/LoveBot/tree/master>")
    
@bot.command()
@asyncio.coroutine
def aide():
    """Envoie une aide (ou un lien vers)"""
    readmeFile = open('README.md','r')
    readmeList = ''.join(readmeFile.readlines())
    readme = readmeList.split('|')
    for r in readme:
      yield from bot.send_message(_author(),r)
    readmeFile.close()
    yield from bot.send_message(_author(),"Lisible aussi sur <https://framagit.org/FirePowi/LoveBot/blob/master/README.md> (un peu bas)")
    
@bot.command()
@asyncio.coroutine
def credit():
  """Crédite les personnes qui participent ou ont participées à la création, au développement, etc. De LoveBot."""
  yield from bot.send_message(_author(),"Mes créateurices sont :\n\
  Maman Aly et Pawi Powi\nMerci à elleux")


# Informations relatives aux personnes du Discords

@bot.command()
@asyncio.coroutine
def website(*channels : str):
    """Donne le site web d’un·e personne, répertorié·e par LoveBot"""
    message=""
    for chan in channels:
        message += sitew(chan) + "\n"
    yield from bot.say(message)

@bot.command()
@asyncio.coroutine
def ytchannel(*channels : str):
    ytcDao = YoutubeChannelDao()
    message = "Aucune chaîne trouvée pour cet-te utilisateurice."

    for chan in channels:
        if chan[0] != "<":
            username = unidecode(u"{}".format(chan))
            data = ytcDao.get(user=username)
            if data:
              message = "{}: {}".format(chan, data)
        else:
            data = ytcDao.get(user_id=chan)
            message = "{}: {}".format(chan, data)

    yield from bot.say(message)

@bot.command()
@asyncio.coroutine
def add_ytchannel(user=None, user_id=None, link=None):
    """Permet aux modos+ d’ajouter des chaînes youtube"""
    ytcDao = YoutubeChannelDao()
    
    
    rang = _rank()
    
    if rang<=2:
      if(user[0] != '<' and user_id[0] == '<' and link[0] == '<'):
        channel = {'user':user,'user_id':user_id,'link':link}
        if(ytcDao.push(channel)):
          yield from bot.send_message(_author(),'Je viens d’ajouter la chaîne que vous m’avez demandé.')
        else:
          yield from bot.send_message(_author(),"Il y a déjà une chaîne pour cet utilisateurice.")
      else:
        yield from bot.send_message(_author(),'Vous avez mal entré.\n\
Usage :\n\
  `!add_ytchannel nom @nom <chaîne youtube>`')
    else:
      yield from bot.send_message(_author(),'Vous n’avez pas le rang nécessaire.')
      


# Extra

@bot.command()
@asyncio.coroutine
def roll(dice : str):
    """Fait rouler des dés au format XdX (par exemple, "!roll 2d6" pour faire rouler 2 dés à 6 faces)."""
    try:
        rolls, limit = map(int, dice.split('d'))
    except Exception:
        yield from bot.say('Le format doit être XdX!')
        return
    if(rolls <= 200 and limit <= 200) :
        result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
        yield from bot.say(result)
    else :
        yield from bot.say("Vous dépassez les bornes !!")
        
@bot.command()
@asyncio.coroutine
def choisis(*choices : str):
    """Fait un choix parmis plusieurs (par exemple : "!choisis bleu rouge orange" choisira au hasard entre ces trois couleurs.."""
    salons = ["264071348395966465","311203817322577920"]
    if _channel().id in salons:
      yield from bot.say(random.choice(choices))
    else:
      salon_names = []
      for s in salons:
        salon_names.append(bot.get_channel(s).name)
      yield from bot.send_message(_author(),"Je n’agis que sur les salons : " + ' ; '.join(salon_names))

@bot.command()
@asyncio.coroutine
def say(channel, message):
  if _rank() <= 2:
    print(_rank())
    yield from asyncio.sleep(0.5)
    yield from bot.send_typing(bot.get_channel(channel))
    yield from asyncio.sleep(len(message)/30.)
    message += " – *" + _author().name + "*"
    yield from bot.send_message(bot.get_channel(channel),message)
    yield from bot.send_message(_author(),'J’ai bien envoyé votre message, chef·fe !')
  else:
    yield from bot.send_message(_author(),'Vous n’avez pas le rang nécéssaire pour faire ça. Il faut au moins être Demi-Dieuxe."')

"""
@bot.group(pass_context=True)
@asyncio.coroutine
def cool(ctx):
    ""Dit si un membre est cool ou non (Utilisation : "!cool MEMBRE").""
    if ctx.invoked_subcommand is None:
        yield from bot.say('Oui, {0.subcommand_passed} est super cool !'.format(ctx))

@cool.command(name="LoveBot")
@asyncio.coroutine
def _LoveBot():
    ""Est-ce que LoveBot est cool ?""
    yield from bot.say('Oui, je suis un bot cool.')

@cool.command(name="Powi")
@asyncio.coroutine
def _Powi():
  ""Est-ce que Powi est cool ?""
  yield from bot.say('Powi est la coolitude absolu. Hey, æl m’a développé, hein !')
"""

if __name__ == "__main__":
    bot.run(token)

