#!/bin/env python3
# -*- coding: utf-8 -*-

def sitew(x):
  site = {
    "<@187361695516065794>"	:	"http://manonminoletti.wixsite.com/manonminoletti",
    "<@203135242813440001>"	:	"http://powi.fr",
    "<@226732347117010944>"	:	"http://hparadoxa.powi.fr",
    "<@279250643665813514>"	:	"http://amour.powi.fr",
    "<@269607597806452738>"	:	"http://fozzy.ovh/",
    ########################################################################################
    "alice"			:	"http://hparadoxa.powi.fr",
    "aly"			:	"http://fozzy.ovh/",
    "caly"			:	"http://fozzy.ovh/",
    "discord"			:	"http://amour.powi.fr",
    "manon"			:	"http://manonminoletti.wixsite.com/manonminoletti",
    "powi"			:	"http://powi.fr",
    "winnie"			:	"http://manonminoletti.wixsite.com/manonminoletti"
    }.get(x.lower(),"Le site de "+x+" n’a pas été trouvée, merci d’envoyer un message à Powi pour lui demander de l’ajouter.")
  if(site != ""):
    return site
